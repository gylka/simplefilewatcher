﻿using System;
using McMaster.Extensions.CommandLineUtils;

namespace SimpleFileWatcher
{
	[Command(ThrowOnUnexpectedArgument = false)]
	class Program
	{
		static void Main(string[] args) => CommandLineApplication.Execute<Program>(args);

		private void OnExecute()
		{
			Log("Hello!");
			Console.ReadKey();
		}

		private static void Log(string message) => Console.WriteLine($"{DateTime.Now:hh:mm:ss}: {message}");
	}
}